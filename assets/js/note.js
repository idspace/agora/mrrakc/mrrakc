document.addEventListener('DOMContentLoaded', function() {
    const notes = document.querySelectorAll('.sliding-note');
    notes.forEach(note => {
      const header = note.querySelector('.note-header');
      header.addEventListener('click', function() {
        note.classList.toggle('active');
      });
    });
  });